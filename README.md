# Python To PDF

## Setup

Install [tesseract](https://github.com/tesseract-ocr/tesseract#installing-tesseract) on your computer:

```shell
sudo apt-get install tesseract-ocr
```

Create a Python virtual environment:

```shell
python3 -m venv venv
pip install -r requirements.txt
```

Running the program:

```shell
python main.py
```

This will bring up a shell with some prompts to follow. It's preferred the file you scan is in the same folder as this project.

## Changelog

### 0.2

- Update dependencies
- Update README instructions

### 0.1

- Can convert PDF to text via CLI
- 2 inputs
  - file name
  - output save directory

## Development

### Executable sources

- [PyInstaller](https://www.pyinstaller.org/)
- [TkInter](https://wiki.python.org/moin/TkInter)
- [WxPython](https://www.wxpython.org/)
- https://realpython.com/python-gui-tkinter/#making-your-applications-interactive (Using TkInter)
- https://medium.com/lifeandtech/executable-gui-with-python-fc79562a5558
- https://stackoverflow.com/questions/2933/create-a-directly-executable-cross-platform-gui-app-using-python
