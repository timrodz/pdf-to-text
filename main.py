import logging
from cli_pdf_converter import PDFConverterCLI

logging.basicConfig(level=logging.INFO)

log = logging.getLogger(__name__)

if __name__ == '__main__':
    log.info(
        """
    🪴🌿🪴🌵🪴🌿🪴🌵🪴🌿🪴🌵🪴🌿🪴🌵🪴🌿🪴🌵🪴🌿🪴
    🌿                                            🌿
    🪴  Welcome to Juan's PDF-to-text converter!  🪴
    🌵                                            🌵
    🪴                     📖                     🪴
    🌿                                            🌿
    🪴  This program uses OCR technology to read  🪴
    🌵  PDF files and turn them into easy-to-use  🌵
    🪴  text files, which your computer can read  🪴
    🌿                                            🌿
    🪴🌿🪴🌵🪴🌿🪴🌵🪴🌿🪴🌵🪴🌿🪴🌵🪴🌿🪴🌵🪴🌿🪴
    """
    )

    pdf_converter = PDFConverterCLI()
    pdf_converter.split_pdf_into_images()
    pdf_converter.convert_images_to_text()
