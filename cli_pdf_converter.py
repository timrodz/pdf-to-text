import os
import cv2
import pytesseract
from pdf2image import convert_from_path
import logging

import image_processor

log = logging.getLogger(__name__)


def convert_image_to_text_io(file, image):
    custom_config = r"--oem 1 --psm 1"
    image_to_string = str(pytesseract.image_to_string(image, lang="eng", config=custom_config))

    text = image_to_string.replace("-\n", "")
    text = text.replace("", "")

    final_text = ""

    for t in text.split("\n"):
        # Bullet points
        if len(t) > 2 and t[0:2] == "e " or t[0:2] == "¢ ":
            final_text += "• " + t[2:] + "\n"
            continue
        final_text += t + "\n"

    file.write(final_text)


class PDFConverterCLI:
    filename: str
    output_dir: str
    image_counter: int = 1

    def __init__(self):
        log.info(
            "🌱 Please type the name of the PDF file you want to process below (i.e. 'file.pdf')"
        )

        filename: str = input("📝 File name: ")
        assert (
                filename.rsplit(".", 1)[1] == "pdf"
        ), "Missing the .pdf extension, please write it with your file (i.e. 'file.pdf')"

        self.filename = filename

        log.info(
            "🌱 Please type the directory where you want to save your results below (i.e. 'output')"
        )
        output_dir: str = input("📝 Directory: ")

        # Make a directory to store all processed files in
        try:
            os.mkdir(path=output_dir)
            self.output_dir = output_dir
        except:
            raise Exception(
                f"Directory '{self.output_dir}' alredy exists. Please delete it, or try a new name"
            )

    def split_pdf_into_images(self) -> None:
        log.info("🎞️ Splitting PDF pages into images, please wait...")

        # Store all the pages of the PDF in a variable
        try:
            pages = convert_from_path(self.filename, dpi=500)
        except:
            raise Exception(f"Could not split {self.filename} into JPEG images")

        # Counter to store images of each page of PDF to image
        i = 1

        # Iterate through all the pages stored above
        for page in pages:
            log.info(f"🎞️ Saving page {i}/{len(pages)}")
            filename = f"{self.output_dir}/page_{i}.jpg"
            page.save(filename, "JPEG")
            log.info(f"🎞️ Page {i}/{len(pages)} converted into JPEG")
            i += 1

        self.image_counter = i
        log.info("🎥 Successfully split PDF pages to individual JPEG files")

    def convert_images_to_text(self) -> None:
        log.info("🎞️ Converting images to text, please wait...")

        # Creating a text file to write the output
        output_filename = f"{self.filename.rsplit('.', 1)[0]}.md"

        # Open the file in append mode so that
        # All contents of all images are added to the same file
        file_thresholding = open(f"{self.output_dir}/{output_filename}", "a")

        # Iterate from 1 to total number of pages
        for i in range(1, self.image_counter):
            # Set filename to recognize text from
            image_filename = f"{self.output_dir}/page_{i}.jpg"

            image_raw = cv2.imread(image_filename)
            grayscale_image = image_processor.get_grayscale(image_raw)
            convert_image_to_text_io(file_thresholding, image_processor.opening(grayscale_image))

        file_thresholding.close()

        log.info(
            f"🎉 Done! Please check the '{os.getcwd()}/{self.output_dir}' directory to see your results.\n👀 They are located inside the file '{output_filename}'"
        )
